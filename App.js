/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import type {Node} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Platform,
  Dimensions,
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
const {width, height} = Dimensions.get('window');

const App: () => Node = () => {
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: '#0f1d21',
        alignItems: 'center',
        justifyContent: 'center',
      }}>
      <View
        style={{
          backgroundColor: 'white',
          alignItems: 'center',
          alignSelf: 'stretch',
          marginHorizontal: width * 0.2,
          height: height * 0.25,
          padding: width * 0.02,
          borderRadius: width * 0.05,
          justifyContent: 'space-around',
        }}>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            // flex:1,
            alignSelf: 'stretch',
            backgroundColor: '#ffffff',
            flex: 1,
            alignSelf: 'stretch',
            borderRadius: width * 0.05,
            justifyContent: 'space-around',
          }}>
          <View
            style={{
              backgroundColor: '#0f1d21',
              width: width * 0.11,
              height: width * 0.11,

              borderRadius: width * 0.02,
            }}></View>
          <Text>name</Text>
          <View
            style={{
              backgroundColor: '#0f1d21',
              width: width * 0.11,
              height: width * 0.11,

              borderRadius: width * 0.02,
            }}></View>
        </View>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            flex: 1,
            alignSelf: 'stretch',
            backgroundColor: '#f4f5fa',

            borderRadius: width * 0.05,
            justifyContent: 'space-around',
          }}>
          <Text>name</Text>
        </View>
      </View>
    </View>
  );
};


export default App;
